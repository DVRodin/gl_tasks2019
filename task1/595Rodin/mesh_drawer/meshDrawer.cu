#include <common/common_functions.cu>
#include <common/CudaError.h>

#include <common/CudaForwardDefines.h>
#include <cuda_runtime_api.h>


__device__ bool intersect(const glm::vec3 pointA, const glm::vec3 pointB, const glm::vec3 pointC, const glm::vec3 viewStart, glm::vec3 viewPoint, glm::vec3& result)
{
	const float epsilon = 1e-4;
	glm::vec3 direction = viewPoint - viewStart;
	
	glm::vec3 norm = glm::cross(pointB - pointA, pointC - pointA);
	float projection = glm::dot(norm, direction);
	if (abs(projection) < epsilon) {
		return false;
	}

	float range = (glm::dot(norm, pointA - viewStart)) / (glm::dot(norm, direction));

	result = viewStart + direction * range;
	projection = glm::dot(norm, pointA - result);
	if (abs(projection) < epsilon) {
		return true;
	}
	else {
		result = viewStart - direction * range;
	}
	return true;
}

__device__ bool isInsideTriangle(const glm::vec3 p1, const glm::vec3 p2, const glm::vec3 p3, const glm::vec3 point) 
{
	const float epsilon = 1e-4;
	float area = glm::length(glm::cross(p2 - p1, p3 - p1)) / 2.0f;

	float area1 = glm::length(glm::cross(point - p1, point - p2)) / 2.0f;
	float area2 = glm::length(glm::cross(point - p2, point - p3)) / 2.0f;
	float area3 = glm::length(glm::cross(point - p3, point - p1)) / 2.0f;

	float symmetricError = 2.0f * abs(area1 + area2 + area3 - area) / (abs(area1 + area2 + area3 + area));

	if (symmetricError < epsilon) {
		return true;
	}
	return false;
}

// It is quite hard to calculate baricentrical coordinates in 3D.
// So normalized inversed lengths are fine too.
__device__ glm::vec3 getCoordinates(const glm::vec3 p1, const glm::vec3 p2, const glm::vec3 p3, const glm::vec3 point) 
{
	const float epsilon = 1e-4;
	float length1 = 1.0 / (glm::length(p1 - point) + epsilon);
	float length2 = 1.0 / (glm::length(p2 - point) + epsilon);
	float length3 = 1.0 / (glm::length(p3 - point) + epsilon);

	float sum = length1 + length2 + length3;
	glm::vec3 result(length1 / sum, length2 / sum, length3 / sum);

	assert(abs(result.x + result.y + result.z - 1.0f) < 1e-4);

	return result;
}

__device__ bool isOnDirection(const glm::vec3 l1, const glm::vec3 l2, const glm::vec3 point) 
{
	return glm::dot(l2 - l1, point - l1) > 0;
}

__device__ bool processPoint( glm::vec3* triangle, glm::vec3 canvasPoint, glm::vec3& intersection) 
{
	glm::vec3 viewPoint(canvasPoint.x, canvasPoint.y, canvasPoint.z + 1); // Alogside Z-
	bool isIntersecting = intersect(triangle[0], triangle[1], triangle[2], viewPoint, canvasPoint, intersection);
	if (!isIntersecting) {
		return false;
	}
	if (!isInsideTriangle(triangle[0], triangle[1], triangle[2], intersection)) {
		return false;
	}
	if (!isOnDirection(viewPoint, canvasPoint, intersection)) {
		return false;
	}
	return true;
}


__global__ void _paintFigureTrianglesSequntial(int trianglesCount, glm::vec3* triangles, glm::u8vec4* colors, glm::vec3* canvasPoints,
	glm::u8vec4* dst_image, ImageWorkArea srcWorkArea, ImageWorkArea dstWorkArea)
{
	glm::ivec2 workPixel(threadIdx.x + blockDim.x * blockIdx.x, threadIdx.y + blockDim.y * blockIdx.y);

	int srcOffset = count_img_offset(workPixel, srcWorkArea);
	int dstOffset = count_img_offset(workPixel, dstWorkArea);

	if (srcOffset < 0 || dstOffset < 0) {
		return;
	}

	glm::vec3 canvasPoint = canvasPoints[srcOffset];
	dst_image[dstOffset] = glm::u8vec4(255, 255, 255, 255);

	float minDistance = 1e8;	
	for (int i = 0; i < trianglesCount; i++) {\
		glm::vec3 intersection;
		if(!processPoint(triangles + 3 * i, canvasPoint, intersection)) {
			continue;
		}
		float distance = glm::length(canvasPoint - intersection);

		if (distance < minDistance) {
			minDistance = distance;
			glm::vec3 weights = getCoordinates(triangles[3 * i], triangles[3 * i + 1], triangles[3 * i + 2], intersection);
			float red = glm::dot(weights, glm::vec3(colors[i * 3].x, colors[i * 3 + 1].x, colors[i * 3 + 2].x));
			float green = glm::dot(weights, glm::vec3(colors[i * 3].y, colors[i * 3 + 1].y, colors[i * 3 + 2].y));
			float blue = glm::dot(weights, glm::vec3(colors[i * 3].z, colors[i * 3 + 1].z, colors[i * 3 + 2].z));
			float alpha = glm::dot(weights, glm::vec3(colors[i * 3].a, colors[i * 3 + 1].a, colors[i * 3 + 2].a));
			dst_image[dstOffset] = glm::u8vec4(red, green, blue, alpha);
		}
	}
	
}

void paintFigureSequential(int trianglesCount, glm::vec3* triangles, glm::u8vec4* colors, glm::vec3* canvasPoints, 
	glm::u8vec4* dst_image, ImageWorkArea srcWorkArea, ImageWorkArea dstWorkArea, glm::uvec2 blockDim)
{
	assert(glm::all(glm::equal(srcWorkArea.workDim, dstWorkArea.workDim)));
	dim3 blkDim(blockDim.x, blockDim.y, 1);
	glm::uvec2 gridDim = glm::uvec2(glm::ivec2(srcWorkArea.workDim + blockDim) - 1) / blockDim;
	dim3 grdDim = { gridDim.x, gridDim.y, 1 };
	// 
	_paintFigureTrianglesSequntial <<<grdDim, blkDim>>> (trianglesCount, triangles, colors, canvasPoints, dst_image, srcWorkArea, dstWorkArea);
	checkCudaErrors(cudaGetLastError());
}

__global__ void _paintFigureParralelFirstRun(int trianglesCount, glm::vec3* triangles, glm::vec3 leftTop,
	glm::vec3 rightBottom, glm::vec2 shifts, int* distances, ImageWorkArea dstWorkArea)
{
	int workTriangle = threadIdx.x + blockDim.x * blockIdx.x;
	if (workTriangle >= trianglesCount) {
		return;
	}
	glm::vec3* triangle = triangles + 3 * workTriangle;

	// locating the projection bounding box of the trianle
	float xMin = glm::min(glm::min(triangle[0].x, triangle[1].x), triangle[2].x);
	float yMin = glm::min(glm::min(triangle[0].y, triangle[1].y), triangle[2].y);
	float xMax = glm::max(glm::max(triangle[0].x, triangle[1].x), triangle[2].x);
	float yMax = glm::max(glm::max(triangle[0].y, triangle[1].y), triangle[2].y);
	// rasterizing the bounding box
	int xStart = glm::ceil((xMin - leftTop.x) / shifts.x);
	int yStart = glm::ceil((yMin - leftTop.y) / shifts.y);
	int xDelta = glm::round((xMax - xMin) / shifts.x);
	int yDelta = glm::round((yMax - yMin) / shifts.y);

	float shiftZ = glm::min(shifts.x, shifts.y);

	for (int x = xStart; x <= xDelta + xStart; x++) {
		for (int y = yStart; y < yDelta + yStart; y++) {
			glm::vec3 canvasPoint(leftTop.x + x * shifts.x, leftTop.y + y * shifts.y, leftTop.z);

			glm::ivec2 workPixel(x, y);

			int dstOffset = count_img_offset(workPixel, dstWorkArea);

			if (dstOffset < 0) {
				continue;
			}

			glm::vec3 intersection;
			if (!processPoint(triangle, canvasPoint, intersection)) {
				continue;
			}
			float distance = glm::length(canvasPoint - intersection);
			int intDistance = glm::round(distance / shiftZ);
			atomicMin(&distances[dstOffset], intDistance);
		}
	}
}

__global__ void _paintFigureParralelSecondRun(int trianglesCount, glm::vec3* triangles, glm::u8vec4* colors, glm::vec3 leftTop,
	glm::vec3 rightBottom, glm::vec2 shifts, int* distances, glm::u8vec4* dst_image, ImageWorkArea dstWorkArea)
{
	int workTriangle = threadIdx.x + blockDim.x * blockIdx.x;
	if (workTriangle >= trianglesCount) {
		return;
	}
	glm::vec3* triangle = triangles + 3 * workTriangle;
	glm::u8vec4* workingColors = colors + 3 * workTriangle;

	// locating the projection bounding box of the trianle
	float xMin = glm::min(glm::min(triangle[0].x, triangle[1].x), triangle[2].x);
	float yMin = glm::min(glm::min(triangle[0].y, triangle[1].y), triangle[2].y);
	float xMax = glm::max(glm::max(triangle[0].x, triangle[1].x), triangle[2].x);
	float yMax = glm::max(glm::max(triangle[0].y, triangle[1].y), triangle[2].y);
	// rasterizing the bounding box
	int xStart = glm::ceil((xMin - leftTop.x) / shifts.x);
	int yStart = glm::ceil((yMin - leftTop.y) / shifts.y);
	int xDelta = glm::round((xMax - xMin) / shifts.x);
	int yDelta = glm::round((yMax - yMin) / shifts.y); 

	float shiftZ = glm::min(shifts.x, shifts.y);

	for (int x = xStart; x <= xDelta + xStart; x++) {
		for (int y = yStart; y < yDelta + yStart; y++) {
			glm::vec3 canvasPoint(leftTop.x + x * shifts.x, leftTop.y + y * shifts.y, leftTop.z);

			glm::ivec2 workPixel(x, y);

			int dstOffset = count_img_offset(workPixel, dstWorkArea);

			if (dstOffset < 0) {
				continue;
			}

			glm::vec3 intersection;
			if (!processPoint(triangle, canvasPoint, intersection)) {
				continue;
			}
				
			glm::vec3 weights = getCoordinates(triangle[0], triangle[1], triangle[2], intersection);

			float red = glm::dot(weights, glm::vec3(workingColors[0].x, workingColors[1].x, workingColors[2].x));
			float green = glm::dot(weights, glm::vec3(workingColors[0].y, workingColors[1].y, workingColors[2].y));
			float blue = glm::dot(weights, glm::vec3(workingColors[0].z, workingColors[1].z, workingColors[2].z));
			float alpha = glm::dot(weights, glm::vec3(workingColors[0].a, workingColors[1].a, workingColors[2].a));
				

			float distance = glm::length(canvasPoint - intersection);
			int intDistance = glm::round(distance / shiftZ);
			if (intDistance == distances[dstOffset]) {
				dst_image[dstOffset] = glm::u8vec4(red, green, blue, alpha);
			}
		}
	}
}

void paintFigureParallel(int trianglesCount, glm::vec3* triangles, glm::u8vec4* colors, int canvasSize,
	glm::vec3 leftTop, glm::vec3 rightBottom, glm::vec2 shifts, int* distances,
	glm::u8vec4* dst_image, ImageWorkArea dstWorkArea, glm::uvec2 blockDim)
{
	// array is flat, so ignoring y
	dim3 blkDim(blockDim.x, 1, 1);
	glm::uvec2 gridDim = glm::uvec2(glm::ivec2(trianglesCount + blockDim.x) - 1) / blockDim.x;
	dim3 grdDim = { gridDim.x, gridDim.y, 1 };

	_paintFigureParralelFirstRun<<<grdDim, blkDim>>>(trianglesCount, triangles, leftTop, rightBottom, shifts, distances, dstWorkArea);
	_paintFigureParralelSecondRun<<<grdDim, blkDim>>> (trianglesCount, triangles, colors, leftTop, rightBottom, shifts, distances, dst_image, dstWorkArea);
	checkCudaErrors(cudaGetLastError());
}
