#pragma once

#include <glm/glm.hpp>
#include <common/common_functions.cuh>

void paintFigureSequential(int trianglesCount, glm::vec3* triangles, glm::u8vec4* colors, glm::vec3* canvasPoints,
	glm::u8vec4* dst_image, ImageWorkArea srcWorkArea, ImageWorkArea dstWorkArea, glm::uvec2 blockDim);

void paintFigureParallel(int trianglesCount, glm::vec3* triangles, glm::u8vec4* colors, int canvasSize,
	glm::vec3 leftTop, glm::vec3 rightBottom, glm::vec2 shifts, int* distances,
	glm::u8vec4* dst_image, ImageWorkArea dstWorkArea, glm::uvec2 blockDim);