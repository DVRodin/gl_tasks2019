#include <cudasoil/cuda_soil.h>
#include <common/CudaImage.h>
#include <common/CudaTimeMeasurement.h>
#include <common/CudaInfo.h>
#include <vector>

#include "meshDrawer.cuh"
#include <reader/meshReader.h>
#include <generator/meshGenerator.h>






int main(int argc, char* argv[]) 
{
	bool isReading = false;
	
	int trianglesCount;
	int canvasWidth;
	int canvasHeight;

	int rowSize = 200;
	float start = -5;
	float end = 5;
	float shift = (end - start) / rowSize;
	canvasHeight = 4096;
	canvasWidth = 4096;
	trianglesCount = rowSize * rowSize;
	int canvasSize = canvasWidth * canvasHeight;

	auto triangles = std::make_shared<CudaResourceManaged<glm::vec3>>(trianglesCount * sizeof(glm::vec3) * 3);
	auto colors = std::make_shared<CudaResourceManaged<glm::u8vec4>>(trianglesCount * sizeof(glm::u8vec4) * 3);
	auto canvas = std::make_shared<CudaResourceManaged<glm::vec3>>(canvasHeight * canvasWidth * sizeof(glm::vec3));

	generateTriangles(start, end, rowSize, triangles->getHandle(), colors->getHandle());

	auto distances = std::make_shared<CudaResourceManaged<int>>(canvasSize * sizeof(int));

	for (int i = 0; i < canvasSize; i++) {
		distances->getHandle()[i] = static_cast<int>(1e20);
	}
	auto dstImage = std::make_shared<CudaImage4c>(canvasWidth, canvasHeight, ResourceLocation::Device);
	ImageWorkArea workArea(glm::uvec2(canvasWidth, canvasHeight));
	glm::uvec2 blockSize = { 1024,1 };


	glm::vec3 leftTop(start, start, 5);
	glm::vec3 rightBottom(end, end, 5);
	glm::vec2 shifts((end - start) / canvasWidth, (end - start) / canvasHeight);

	cudaDeviceSynchronize();
	paintFigureParallel(trianglesCount, triangles->getHandle(), colors->getHandle(), canvasSize, leftTop, rightBottom, shifts, distances->getHandle(), dstImage->getHandle(), workArea, blockSize);
	cudaDeviceSynchronize();
	save_image_rgb("595RodinData1/random.png", dstImage);
	return 0;
}
