file(GLOB_RECURSE mesh_drawer_SOURCES
        ${CMAKE_CURRENT_SOURCE_DIR}/*.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/*.h
        ${CMAKE_CURRENT_SOURCE_DIR}/*.cu
        ${CMAKE_CURRENT_SOURCE_DIR}/*.cuh
        )
cuda_add_executable(mesh_drawer ${mesh_drawer_SOURCES})

target_link_libraries(mesh_drawer cuda_samples_common cudasoil)

install(TARGETS mesh_drawer RUNTIME DESTINATION ${CMAKE_INSTALL_PREFIX})
