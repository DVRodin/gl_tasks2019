#include "meshReader.h"
#include <fstream>
#include <assert.h>
#include <generator/meshGenerator.h>

void CMeshReader::ReadData(glm::vec3* triangles, glm::u8vec4* colors, glm::vec3* canvas)
{
	assert(hasReadSettings);
	std::ifstream dataFile;
	dataFile.open(file);
	int temp;
	dataFile >> temp >> temp >> temp;

	for (int i = 0; i < fileTrianglesCount; i++) {
		for (int vertex = 0; vertex < 3; vertex++) {
			float x, y, z;
			int r, g, b, a;
			dataFile >> x >> y >> z >> r >> g >> b >> a;
			triangles[i * 3 + vertex] = glm::vec3(x, y, z);
			colors[i * 3 + vertex] = glm::u8vec4(r, g, b, a);
		}
	}

	float startX, endX, startY, endY, z;
	dataFile >> startX >> endX >> startY >> endY >> z;

	generateCanvas(startX, endX, startY, endY, z, canvasWidth, canvasHeigth, canvas);
	dataFile.close();
}

void CMeshReader::ReadSettings(int& trianglesCount, int& width, int& height)
{
	std::ifstream dataFile(file);
	dataFile >> fileTrianglesCount >> canvasWidth >> canvasHeigth;
	trianglesCount = fileTrianglesCount;
	width = canvasWidth;
	height = canvasHeigth;
	hasReadSettings = true;
	dataFile.close();
}
