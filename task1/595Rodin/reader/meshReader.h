#pragma once
#include <string>
#include <cuda_runtime_api.h>
#include <glm/glm.hpp>


class CMeshReader {
public:
	CMeshReader(std::string _file) : file(_file) {}
	void ReadData(glm::vec3* triangles, glm::u8vec4* colors, glm::vec3* canvas);
	void ReadSettings(int& trianglesCount, int& width, int& height);

private:
	std::string file;

	int fileTrianglesCount;
	int canvasWidth;
	int canvasHeigth;

	bool hasReadSettings = false;
};