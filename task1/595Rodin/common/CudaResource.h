#pragma once

#include <cuda_runtime_api.h>
#include "CudaError.h"

enum class ResourceLocation {
	Device,
	Host
};

template <typename T>
class CudaResource {
public:
	CudaResource(T* _handle, size_t _size, ResourceLocation _location) : location(_location), handle(_handle), size(_size) { }

	CudaResource(size_t _size, ResourceLocation _location) : location(_location), size(_size) {
		if (location == ResourceLocation::Device) {
			checkCudaErrors(cudaMalloc(reinterpret_cast<void**>(&handle), _size));
		}
		else {
			checkCudaErrors(cudaMallocHost(reinterpret_cast<void**>(&handle), _size));
		}
	}

	virtual ~CudaResource() {
		if (location == ResourceLocation::Device) {
			checkCudaErrors(cudaFree(handle));
		}
		else {
			checkCudaErrors(cudaFreeHost(handle));
		}
	}

	T* getHandle() const { return handle; }

	size_t getSize() const { return size; }

private:
	ResourceLocation location = ResourceLocation::Device;
	T *handle = nullptr;
	size_t size = 0;
};

template <typename T>
class CudaResourceManaged {
public:
	CudaResourceManaged(T* _handle, size_t _size) : handle(_handle), size(_size) { }

	CudaResourceManaged(size_t _size) : size(_size) {
		checkCudaErrors(cudaMallocManaged(reinterpret_cast<void**>(&handle), _size));
	}

	virtual ~CudaResourceManaged() {
		checkCudaErrors(cudaFree(handle));
	}

	T* getHandle() const { return handle; }

	size_t getSize() const { return size; }

private:
	T *handle = nullptr;
	size_t size = 0;
};