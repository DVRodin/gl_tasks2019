#include "common_functions.cuh"

#include <vector_types.h>
#include <vector_functions.h>
#include <device_atomic_functions.h>
#include <device_launch_parameters.h>

// Counts 2D pixel 1D offset for image stored as array.
__device__ static inline int count_img_offset(glm::uvec2 workPixel, ImageWorkArea _area) {
	if (glm::any(glm::lessThan(workPixel, glm::uvec2(0))) ||
			glm::any(glm::greaterThanEqual(workPixel, _area.workDim))) {
		return -1;
	}
	glm::uvec2 imagePixel = workPixel + _area.offset;
	if (glm::any(glm::greaterThanEqual(imagePixel, _area.imageDim))) {
		return -1;
	}

	int offset = imagePixel.y * _area.imageDim.x + imagePixel.x;
	return offset;
}


