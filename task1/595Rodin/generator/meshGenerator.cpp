#include "meshGenerator.h"
#include <random>


void generateTriangles(float start, float end, int rowSize, glm::vec3* triangles, glm::u8vec4* colors)
{
	float shift = (end - start) / rowSize;
	float shiftZ = -1 * shift / 2;
	for (float i = 0; i < rowSize; i++) {
		for (float j = 0; j < rowSize; j++) {
			int current = i * rowSize + j;
			triangles[3 * current] = glm::vec3(start + (j - 1) * shift, start + (i - 1) * shift, j * shiftZ);
			triangles[3 * current + 1] = glm::vec3(start + (j + 1) * shift, start + i * shift, 0);
			triangles[3 * current + 2] = glm::vec3(start + j * shift, start + (i + 1) * shift, 0);
			for (int vertex = 0; vertex < 3; vertex++) {
				int r = rand() % 256;
				int g = rand() % 256;
				int b = rand() % 256;
				colors[3 * current + vertex] = glm::u8vec4(r, g, b, 255);
			}
		}
	}
}

void generateCanvas(float startX, float endX, float startY, float endY, float z, int canvasWidth, int canvasHeigth, glm::vec3* canvas)
{
	float deltaX = abs(endX - startX) / canvasWidth;
	float deltaY = abs(endY - startY) / canvasHeigth;

	for (int y = 0; y < canvasHeigth; y++) {
		for (int x = 0; x < canvasWidth; x++) {
			canvas[y * canvasWidth + x] = glm::vec3(startX + x * deltaX, startY + y * deltaY, z);
		}
	}
}