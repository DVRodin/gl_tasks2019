#pragma once
#include <glm/glm.hpp>

void generateTriangles(float start, float end, int rowSize, glm::vec3* triangles, glm::u8vec4* colors);

void generateCanvas(float startX, float endX, float startY, float endY, float z, int canvasWidth, int canvasHeigth, glm::vec3* canvas);
